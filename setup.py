from distutils.core import setup

setup(
    name = "django-accounts",
    version = "0.0.1",
    description = "Custom User Model For Django",
    packages = [
        "accounts",
    ],
    classifiers = [
        'Programming Language :: Python',
        'Operating System :: OS Independent',
        'Framework :: Django',
    ],
)