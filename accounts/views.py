from django.views.generic import TemplateView
from django.views.decorators.csrf import ensure_csrf_cookie
from django.contrib.auth.decorators import login_required

class ProfileView(TemplateView):
    template_name = "accounts/profile_view.html"

profile_view = login_required(ensure_csrf_cookie(ProfileView.as_view()))